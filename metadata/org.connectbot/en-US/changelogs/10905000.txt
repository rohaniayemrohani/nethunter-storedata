· Various bug fixes.
· Support for newer SSH authentication methods.
· Support for resizing on Chrome OS.
